00:14 Traditional - Greensleeves
00:32 Handel - Messiah - Hallelujah chorus
00:37 Mozart - Eine Kleine Nachtmusik
00:43 Beethoven - Symphony No. 9 - "Ode to Joy"
00:54 Rossini - William Tell Overture
01:00 Puccini - Turandot - Nessun Dorma
01:17 Bach - Prelude in C Major
01:27 Beethoven - Symphony No. 5
01:35 Strauss - Also Sprach Zarathustra
01:48 Offenbach - Orpheus in the Underworld
01:53 Bizet - Carmen - Toreador Song
01:59 Brahms - Hungarian Dance #5 (in F#m)
02:04 Tchaikovsky - Nutcracker Suite - Dance of the Sugar Plum Fairies
02:12 Grieg - Peer Gynt Suite - In the Hall of the Mountain King
02:18 Rimsky-Korsakov-Flight of the Bumblebee (underneath previous song)
02:23 The James Bond Theme
02:28 Grieg-Peer Gynt Suite
02:35 Scott Joplin-The Entertainer
02:40 Brown / Freed-Singin\' In The Rain
02:47 Sound of Music-Do Re Mi
02:58 Irving Berlin-There\'s No Business Like Show Business
03:07 falling scale
03:09 George Gershwin-Summertime
03:24 Glen Miller - In The Mood
03:34 Kander,Ebb-New York, New York
03:40 Elvis Presley-Hound Dog
03:53 Roy Orbison-Pretty Woman
04:08 McCartney / Lennon-Yesterday
04:17 James Brown-I Feel Good
04:23 The Bee Gees-Stayin\' Alive
04:31 falling scale
04:34 Abba-Money, Money, Money
04:46 Queen-Another One Bites The Dust
04:52 Eric Clapton-Layla
05:00 Michael Jackson-Billy Jean
05:07 Deep Purple-Smoke On The Water
05:14 Theme from Jaws
05:21 Knife Scene from Hitchcock\'s Psycho
05:30 Wagner-Ride of the Valkyries (movie Apocalypse Now)
05:33 Jerrold Immel-Theme to Dallas
05:36 Thomas Augustine Arne-Rule Brittania
05:38 Traditional-Sailor\'s Hornpipe (WB cartoon Buccaneer Bunny)
05:40 Traditional-Shave And A Haircut (WB Cartoon Looney Tunes)
05:42 Wagner-Lohengrin-Wedding Chorus
