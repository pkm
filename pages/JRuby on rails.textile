http://wiki.jruby.org/wiki/Jruby_on_Rails_on_Tomcat

h3. 安装JRuby


h3. jruby script/server

安装需要的gem, redcloth, mini_magick等

修改envirnment.rb，增加

{{{
config.gem "RedCloth", :version => "= 4.1.9", :lib => 'redcloth'
config.gem "mini_magick"
}}}


h3. 安装使用warbler

{{{
jruby -S gem install warbler
jruby -S warble config

#add a line to config/warble.rb:

config.gems += ["activerecord-jdbcmysql-adapter", "jruby-openssl"]  #把需要的gems都往这里放吧


# set the username and password in config/database.yml "production" section, as Tomcat will use production mode automatically:

adapter: jdbcmysql

warble war 

}}}


h3. 部署


修改context root为/

copy原来在public下的内容回到web-inf目录下的public下，warbler没有这么做。
