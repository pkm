* {{restful-authentication}}

* {{JRuby on rails}}

* Rails Log config

{{{
RAILS_DEFAULT_LOGGER.level = Logger::FATAL
}}}

* Keyword.connection().execute(cmd)



* config.gem
{{{(ruby)
Rails::Initializer.run do |config| 

  # Require the latest version of haml
  config.gem "haml"

  # Require a specific version of chronic
  config.gem "chronic", :version => '0.2.3'

  # Require a gem from a non-standard repo
  config.gem "hpricot", :source => "http://code.whytheluckystiff.net"

  # Require a gem that needs to require a file different than the gem's name
  # I.e. if you normally load the gem with require 'aws/s3' instead of
  # require 'aws-s3' then you would need to specify the :lib option
  config.gem "aws-s3", :lib => "aws/s3" 
end 
}}}

* {{升级Rails2.3.2}}
* {{Date format}}
* http://wiki.rubyonrails.org/rails/pages/RequireDependency
* 如果session怎么设置都是nil，那么要考虑下session_domain
* "files"=>{"1"=>"a", "2"=>"b", "3"=>"c"} 
{{{
<input type="text" name="files[1]" />
<input type="text" name="files[2]" />
<input type="text" name="files[3]" />
}}}


h3. Rails中获取 HTTP_REFERER
{{{

request.env["HTTP_REFERER"]

可以取到的参数包括：

SERVER_NAME: localhost
PATH_INFO: /forum/forums
HTTP_USER_AGENT: Mozilla/5.0 (Macintosh; U; Intel Mac OS X; en) AppleWebKit/522.11 (KHTML, like Gecko) Version/3.0.2 Safari/522.12
HTTP_ACCEPT_ENCODING: gzip, deflate
SCRIPT_NAME: /
SERVER_PROTOCOL: HTTP/1.1
HTTP_HOST: localhost:3000
HTTP_CACHE_CONTROL: max-age=0
HTTP_ACCEPT_LANGUAGE: en
REMOTE_ADDR: 127.0.0.1
SERVER_SOFTWARE: Mongrel 1.1.3
REQUEST_PATH: /forum/forums
HTTP_REFERER: http://localhost:3000/
HTTP_COOKIE: _matchsession=BAh7BzoMY3NyZl9pZCIlNWJiNzg4NDUzOWQzNWFhZTQ4MGRkNTUwYzc0MDc5%250AZGYiCmZsYXNoSUM6J0FjdGlvbkNvbnRyb2xsZXI6OkZsYXNoOjpGbGFzaEhh%250Ac2h7AAY6CkB1c2VkewA%253D268e6091590591d959128f3b17b62ff46244a0a3; _slemail=temp%40email.com; _slhash=9dfd86431742273e3e96e06a1c20541d69f74dc9; _haha_session=BAh7BiIKZmxhc2hJQzonQWN0aW9uQ29udHJvbGxlcjo6Rmxhc2g6OkZsYXNo%250ASGFzaHsABjoKQHVzZWR7AA%253D%253D--96565e41694dc839bd244af40b5d5121a923c8e3
HTTP_VERSION: HTTP/1.1
REQUEST_URI: /forum/forums
SERVER_PORT: "3000"
GATEWAY_INTERFACE: CGI/1.2
HTTP_ACCEPT: text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5
HTTP_CONNECTION: keep-alive
REQUEST_METHOD: GET
}}}


h2. select on change
{{{(ruby)
<%= select :exam, :display, [['单题显示', 'single'], ['分题型显示', 'section'], ['全部显示', 'all']], {}, :onchange => "alert(this.value)"  %> 
}}}


h2. Returning

{{{(ruby)
def full_name(user)
  user = current_user unless user

  name = user.first_name
  name << ' ' + user.middle_name unless user.middle_name.blank?
  name << ' ' + user.last_name
  name
end

#equal below

def full_name(user)
  user = current_user unless user

  returning String.new do |name|
    name << user.first_name
    name << ' ' + user.middle_name unless user.middle_name.blank?
    name << ' ' + user.last_name
  end
end

}}}

h2. method_missing

{{{(ruby)
def method_missing(method_sym, *arguments, &block)
    if method_sym.to_s =~ /^user_answer_\d+$/
      send(:user_answer, method_sym.to_s.gsub("user_answer_", ""))
    else
      super
    end
  end
  
  def respond_to?(method_sym, include_private = false)
    if method_sym.to_s =~ /^user_answer_\d+$/
      true
    else
      super
    end
  end
}}}


h2. 单独使用activerecord
{{{
   1. require 'rubygems'    
   2. require 'active_record'    
   3. require 'yaml'    
   4. require 'logger'    
   5.     
   6. dbconfig = YAML::load(File.open('database.yml'))    
   7. ActiveRecord::Base.establish_connection(dbconfig)    
   8. ActiveRecord::Base.logger = Logger.new(File.open('database.log', 'a'))    
   9.     
  10. class User < ActiveRecord::Base  
  11.    set_table_name "my_user"  
  12. end    
  13.     
  14. puts User.count    
}}}
